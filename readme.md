## Jenkins实验

Jenkins 是一种开源的持续集成（Continuous Intergration, CI）工具，支持

- 持续且自动构建/测试软件项目
- 监控软件开放流程，便于问题的定位和提示，可提高开发效率

在node2上安装适配CentOS的Jenkins，并进行Pipeline实验

node2操作系统：CentOS

Jenkins: LTS

https://www.jenkins.io/doc/book/installing/#red-hat-centos

需要有root权限

### 安装

```bash
sudo wget -O /etc/yum.repos.d/jenkins.repo \
    https://pkg.jenkins.io/redhat/jenkins.repo
sudo rpm --import https://pkg.jenkins.io/redhat/jenkins.io.key
sudo yum upgrade
sudo yum install jenkins java-1.8.0-openjdk-devel
```

### Start Jenkins

- 启动jenkins

```bash
sudo systemctl start jenkins
```

- 查看jenkins状态

```bash
sudo systemctl status jenkins
```

虽然启动时没有报错，但状态显示为“exited”，正常情况应该是“running”

![](figures/exited-error.jpg)

由于默认jenkins的打开端口为8080,存在端口冲突的情况

(查看防火墙时，发现8080端口作为tcp端口向外开放了)

以root权限打开文件`sudo vim /etc/sysconfig/jenkins`, 修改JENKINS_PORT="8088"，即端口改为8088

关闭之前打开的jenkins，并重新打开：

```bash
sudo systemctl stop jenkins
sudo systemctl start jenkins 
sudo systemctl status jenkins
```

![](figures/jenkins-running.jpg)

此时还不能通过外部访问，因为服务器设置了防火墙。

需要设置将以上设置的端口8088作为开放端口。

```bash
sudo firewall-cmd --zone=public --add-service=http --permanent
sudo firewall-cmd --reload
```

### 登录jenkins

在外部电脑上，访问jenkins：ip-of-node2:8088

要求输入密码，在服务器中

```bash
sudo cat /var/lib/jenkins/secrets/initialAdminPassword
```

即可将密码打印到终端

按照要求输入密码，登录进去

![](figures/login.jpg)

没有特别的需求，点击左边的“Install suggested plugins”，等待安装结束。

暂时跳过了后面的 **Create First Admin User**，和**Configurate the Jenkins URL**

后期登录管理员账号为默认

```
username: admin
password: 前面提到的进入setup wizard界面的密码
```

![](figures/finish_start.png)

进入Jenkins界面

![](figures/jenkins.jpg)

### 创建第一个Pipeline：Classic UI

- 点击”New Item"

- 输入项目名称“hello_world"

- 类型选择：”Pipeline“

- 在Pipeline中输入jenkinsfile的内容

![](figures/jenkinsfile.jpg)

- "Save"
- "Build Now"

- Console output

![](figures/output.jpg)

### 创建Pipeline：SCM

以从gitlab仓库构建自动测试pipeline 为例

要求jenkins所在机器安装有git

通过SCM创建pipeline参考：https://www.jenkins.io/doc/book/pipeline/getting-started/#defining-a-pipeline-in-scm

仓库位置：https://gitlab.com/FuzuoZhang/jenkins

需要配置gitlab账号的凭证：https://www.cnblogs.com/dotnet261010/p/12393917.html

![](figures/scm.jpg)

Jenkinsfile 内容

![](figures/jenkinsfile_scm.jpg)

Build 结果

![](figures/scm_output.jpg)

### 创建Pipeline：Blue Ocean

在jenkins中使用Blue Ocean，要求jenkins版本满足2.7.x及以上

查看jenkins版本

```bash
cd /var/lib/jenkins
cat config.xml|grep version
<?xml version='1.1' encoding='UTF-8'?>
  <version>2.235.1</version>
```

安装Blue Ocean插件：https://www.jenkins.io/doc/book/blueocean/getting-started/

选择安装插件blue ocean，选中，“Download now and install afeter restart"

![](figures/blue_ocean.jpg)

等待安装成功后，在jenkins 主页左边会有一个"Open Blue Ocean"的新菜单

如果没有，可以重启Jenkins

```bash
sudo systemctl stop jenkins
sudo systemctl start jenkins
sudo systemctl status jenkins
```

重新打开Jenkins主页，此时会看到Blue Ocean的标志。

可以通过图标打开Blue Ocean的UI界面，也可以直接通过URL索引：http://166.111.72.220:8088/blue

Blue Ocean主界面

![](figures/blue.jpg)

run一次已有的pipeline project

![](figures/blue_ocean_output.jpg)

在Blue Ocean中创建新的pipeline：支持本地仓库和远程仓库，但由于本地仓库会涉及比较复杂的路径设置问题，所以更加鼓励使用远程仓库，并且**Blue Ocean中只支持ssh的Repository URL**。



完成。
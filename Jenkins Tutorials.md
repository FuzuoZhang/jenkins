### Jenkins Tutorials



- Documentation https://www.jenkins.io/doc/
- Guided Tour https://www.jenkins.io/doc/pipeline/tour/hello-world/
- Pipeline https://www.jenkins.io/doc/book/pipeline/



#### Jenkins Pipeline 工作示意图

![](figures/pipeline.jpg)

A Pipeline is a user-defined model of a CD pipeline. A Pipeline’s code defines your entire build process, which typically includes stages for building an application, testing it and then delivering it.



#### Pipeline example

![](figures/pipeline_example.jpg)

Jenkins 核心部分：Pipeline, Blue Ocean

在Jenkins 中建立Pipeline有三种方式：

- classic UI（直接在界面中New Item建立）
- Blue Ocean
- SCM

### 用户管理

"Jenkins">"Configure Global Security">"Authorization">"Matrix-based security"


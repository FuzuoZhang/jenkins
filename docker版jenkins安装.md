初步实验Jenkins，在node2节点上，安装docker形式的jenkins。

### 1. 安装docker

https://docs.docker.com/engine/install/centos/

联网状态下： [install using the repository](https://docs.docker.com/engine/install/centos/#install-using-the-repository)

非联网状态: RPM package

安装到 /var/lib/docker/位置

### 2. 设置非root账号使用docker

```bash
sudo groupadd docker
sudo usermod -aG docker USER_NAME
newgrp docker
docker run hello-world
```

### 3. 下载jenkins image

https://www.jenkins.io/doc/book/installing/#downloading-and-running-jenkins-in-docker

下载已有的jenkins 镜像，并在本地运行，生成本地的container

### 进入容器

`docker container exec -it <docker container name> bash`

docker container ls 查看正在运行的所有container





